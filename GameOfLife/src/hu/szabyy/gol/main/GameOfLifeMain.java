package hu.szabyy.gol.main;

import hu.szabyy.gol.controller.Controller;
import hu.szabyy.gol.view.Window;

/**
 *
 * @author szaby
 */
public class GameOfLifeMain {

  public static void main(String[] args) {
   
   Controller a = Controller.getINSTANCE();
   Window w = new Window(a);
  }
  
}
