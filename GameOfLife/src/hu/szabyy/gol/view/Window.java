package hu.szabyy.gol.view;

import hu.szabyy.gol.controller.Controller;
import hu.szabyy.gol.interfaces.SizeAndScale;
import hu.szabyy.gol.model.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Window extends JFrame implements ActionListener, SizeAndScale, ChangeListener {

  private Timer drawTimer = new Timer(0, this);
  private JToggleButton btAutomatic = new javax.swing.JToggleButton("Automatic");
  private JToggleButton btReverse = new JToggleButton("Back");
  private JButton btOneStep = new JButton("OneStep");
  private JSpinner spinnerScale;
  private PlayingPlace pnPlayingPlace;//= new PlayingPlace();

  private Controller controller;

  private int delayTime = 0;

  private JLabel label1;

  private File lifFile = new File(".");
  private JMenuItem miOpenJMenuItem, miExitJMenuItem;
  private JFileChooser fc = new JFileChooser();

  public Controller getController() {
    return controller;
  }

  private Window() {

  }

  public Window(Controller controller) {
    this.controller = controller;

    this.pnPlayingPlace = new PlayingPlace(this);
    pnPlayingPlace.addKeyListener(pnPlayingPlace);
    pnPlayingPlace.setFocusable(true);

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    this.setAlwaysOnTop(false);
    setTitle("Game Of Life 1.0");
    setSize(900, 900);
    setLocationRelativeTo(this);
    setResizable(false);
    setLayout(new BorderLayout());
    JPanel pnPanel = new JPanel(new FlowLayout());
    this.label1 = new JLabel();
    this.label1.setFont(new Font(Font.DIALOG, Font.BOLD, 20));

    setLabel(0);
    JLabel labelScale = new JLabel("Scale: ");
    labelScale.setFont(new Font(Font.DIALOG, Font.BOLD, 20));

    spinnerScale = (JSpinner) createCreateSpinner();
    spinnerScale.setFont(new Font(Font.DIALOG, Font.BOLD, 20));

    pnPanel.add(label1);
    label1.setFocusable(true);
    label1.requestFocus();
    pnPanel.add(btAutomatic);
    pnPanel.add(btOneStep);
    pnPanel.add(btReverse);
    pnPanel.add(btReverse);
    pnPanel.add(labelScale);
    pnPanel.add(spinnerScale);
    add(pnPanel, BorderLayout.PAGE_START);
    pnPlayingPlace.setBackground(Color.WHITE);

    pnPlayingPlace.addKeyListener(pnPlayingPlace);
    pnPlayingPlace.setFocusable(true);

    add(pnPlayingPlace, BorderLayout.CENTER);

    JMenuBar mbFile = new JMenuBar();
    JMenu mFile = new JMenu("Open");
    setJMenuBar(mbFile);
    mbFile.add(mFile);
    mFile.add(miOpenJMenuItem = new JMenuItem("Open..."));
    miOpenJMenuItem.setIcon(
            new ImageIcon("./images/openfile.jpg"));
    mFile.addSeparator();
    mFile.add(miExitJMenuItem = new JMenuItem("Kilép"));
    miOpenJMenuItem.addActionListener(this);
    miExitJMenuItem.addActionListener(this);
    fc.setFileFilter(new MyFileFilter());
    setVisible(true);

    btAutomatic.addActionListener(this);
    btReverse.addActionListener(this);
    btOneStep.addActionListener(this);
    spinnerScale.addChangeListener(this);
  }

  private void setLabel(Integer e) {
    this.label1.setText("Generation: " + e);
  }

  public void paintGenerations(Generations generations, Color color) {
    Graphics g = pnPlayingPlace.getGraphics();
    g.setColor(color);
    Coordinate newCoordinate;
    int scale = controller.getScaleToView();
    if (generations.getPresentGeneration() != null) {
      for (int i = 0; i < generations.getPresentGeneration().size(); i++) {
        newCoordinate = generations.getPresentGeneration().get(i);

        g.fillRect(X_MAX / 2 + newCoordinate.getX() * scale, Y_MAX / 2 + scale + newCoordinate.getY() * scale, scale, scale);

      }
    }
  }

  public void clearGameTable() {
    Graphics g = pnPlayingPlace.getGraphics();
    g.setColor(Color.WHITE);
    g.fillRect(0, 0, X_MAX, Y_MAX);
    g.setColor(Color.BLACK);
  }

  private void oneStep() {

    paintGenerations(controller.doIt(), Color.WHITE);
    paintGenerations(controller.getGenerations(), Color.BLACK);

    setLabel(controller.getStepCounter());
  }

  private boolean openLifFile() {
    boolean ok = true;
    fc.setCurrentDirectory(new java.io.File("."));
    fc.setDialogTitle("choos any .lif file ");
    if (fc.showOpenDialog(this)
            == JFileChooser.APPROVE_OPTION) {
      miOpenJMenuItem.setEnabled(true);
      lifFile = fc.getSelectedFile();

      try {
        controller.setGenerationsFromFile(lifFile);
        clearGameTable();
        setLabel(0);
      } catch (Exception ex) {
        JOptionPane.showMessageDialog((Component) null, ex.getMessage(),
                "alert", JOptionPane.OK_OPTION);
        ok = false;
      }

    }
    return ok;
  }

  public Component createCreateSpinner() {

    Integer value = 1;
    Integer min = 1;
    Integer max = 10;
    Integer step = 1;

    SpinnerModel model1 = new SpinnerNumberModel(value, min, max, step);
    JSpinner spinner1 = new JSpinner(model1);
    spinner1.setPreferredSize(new Dimension(100, 25));

    return spinner1;
  }

  private void initButtonsEct() {
    btOneStep.setEnabled(true);
    btAutomatic.setEnabled(true);
    btAutomatic.setSelected(false);
    btReverse.setSelected(controller.isReverse());
    btReverse.setEnabled(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == drawTimer) {
      if (controller.getStarted() && !controller.getPaused()) {
        oneStep();
      }
    } else if (e.getSource() == btAutomatic) {

      if (btAutomatic.isSelected()) {
        drawTimer.start();
        controller.setPaused(false);
        btOneStep.setEnabled(false);
        controller.setStarted(true);
        btReverse.setEnabled(false);
      } else {
        drawTimer.stop();
        controller.setPaused(true);
        btOneStep.setEnabled(true);
        btReverse.setEnabled(true);
      }

    } else if (e.getSource() == btOneStep) {

      controller.setStarted(true);
      controller.setPaused(true);
      oneStep();

    } else if (e.getSource() == miOpenJMenuItem) {

      if (openLifFile()) {
        initButtonsEct();
        clearGameTable();
        setLabel(0);
        paintGenerations(controller.getGenerations(), Color.BLACK);
      }

    } else if (e.getSource() == miExitJMenuItem) {
      System.exit(0);
    } else if (e.getSource() == btReverse) {
      controller.setReverse(!controller.isReverse());
      btReverse.setSelected(controller.isReverse());
    }

  }

  @Override
  public void stateChanged(ChangeEvent e) {
    if (e.getSource() == spinnerScale) {
      int newScale = (int) spinnerScale.getValue();
      controller.setScaleToView(newScale);

      clearGameTable();
      if (controller.getPaused() && controller.getGenerations() != null) {
        paintGenerations(controller.getGenerations(), Color.BLACK);
      }
    }
  }

}
