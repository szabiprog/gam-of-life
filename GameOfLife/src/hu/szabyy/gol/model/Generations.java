package hu.szabyy.gol.model;

import java.util.ArrayList;
import java.util.List;

public class Generations {

  private List<Coordinate> presentGeneration;
  private List<Coordinate> dyingGeneration;
  private List<Coordinate> newGeneration;

  public Generations() {
    presentGeneration = new ArrayList<>();
    dyingGeneration = new ArrayList<>();
    newGeneration = new ArrayList<>();
  }

  public List<Coordinate> getPresentGeneration() {
    return presentGeneration;
  }

  public void setPresentGeneration(List<Coordinate> presentGeneration) {
    this.presentGeneration = presentGeneration;
  }

  public List<Coordinate> getDyingGeneration() {
    return dyingGeneration;
  }

  public void setDyingGeneration(List<Coordinate> dyingGeneration) {
    this.dyingGeneration = dyingGeneration;
  }

  public List<Coordinate> getNewGeneration() {
    return newGeneration;
  }

  public void setNewGeneration(List<Coordinate> newGeneration) {
    this.newGeneration = newGeneration;
  }

}
