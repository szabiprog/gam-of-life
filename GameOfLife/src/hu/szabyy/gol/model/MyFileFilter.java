package hu.szabyy.gol.model;


import java.io.File;
import javax.swing.filechooser.FileFilter;

public class MyFileFilter extends FileFilter {                //1

  @Override
  public boolean accept(File f) {
    return (f.getName().toLowerCase().endsWith(".lif"));
  }

  @Override
  public String getDescription() {
    return "*.lif";
  }
}
