package hu.szabyy.gol.model;

public class Coordinate {

  private int x;
  private int y;

  public Coordinate() {

  }

  public Coordinate(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public int getX() {
    return x;
  }

  @Override
  public String toString() {
    return this.x + " " + this.y;
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof Coordinate) {

      if (this.x == ((Coordinate) other).x && this.y == ((Coordinate) other).y) {
        return true;
      }
    }
    return false;
  }

}
