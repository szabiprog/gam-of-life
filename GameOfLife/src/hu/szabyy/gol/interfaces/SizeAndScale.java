/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.szabyy.gol.interfaces;

/**
 *
 * @author szaby
 */
public interface SizeAndScale {
  final int X_MAX=900;
  final int Y_MAX=900;
  
  final int LIFE_PLACE_X_MAX=900;
  final int LIFE_PLACE_Y_MAX=900;
  
  int X_SCALE = 1;
  int Y_SCALE = 1;
  
}
