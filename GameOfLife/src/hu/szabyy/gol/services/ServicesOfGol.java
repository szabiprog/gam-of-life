package hu.szabyy.gol.services;

import hu.szabyy.gol.interfaces.SizeAndScale;
import hu.szabyy.gol.model.Coordinate;
import hu.szabyy.gol.model.Generations;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServicesOfGol {

  public static Generations generationsFromStrin(String str) {
    Generations generations = new Generations();

    return generations;
  }

  public static Generations fileToGenerations(File lifFile) throws Exception {
    Generations generations = new Generations();
    generations.setPresentGeneration(new ArrayList<>());

    BufferedReader br = null;
    String line = "";
    String splitBy = " ";
    boolean firstPoint = false;
    int startX = 0;
    int startY = 0;
    String version = null;
    try {
     br = new BufferedReader(new FileReader(lifFile));
      while ((line = br.readLine()) != null) {
        if (version == null) {
          version = line;
        } else {
          String[] adat = line.split(splitBy);
          if (version.equals("#Life 1.05")) {
            if (firstPoint == true && !adat[0].equals("#P")) {

              StringBuffer pattern = new StringBuffer(adat[0]);
//              System.out.println(pattern);
//              System.out.println(pattern.length());
              startY--;
              for (int i = 0; i < pattern.length(); i++) {
                startX++;
                Character aktualPattern = pattern.charAt(i);
                if (aktualPattern.equals("*".charAt(0))) {
                  Coordinate oneOfGeneration = new Coordinate(startX, startY);
                  generations.getPresentGeneration().add(oneOfGeneration);
//                  System.out.println(startX + " " + startY + "*");
                }
              }
              startX = startX - pattern.length();
            }
            if (adat[0].equals("#P")) {
              firstPoint = true;
              startX = Integer.parseInt(adat[1]) - 1;
              startY = Integer.parseInt(adat[2]) + 1;
//              System.out.println(startX + " " + startY);

            }// "#Life 1.05"
          } else if (version.equals("#Life 1.06")) {
            startX = Integer.parseInt(adat[0]);
            startY = Integer.parseInt(adat[1]);
//            System.out.println(startX + " " + startY);
            Coordinate oneOfGeneration = new Coordinate(startX, startY);
            generations.getPresentGeneration().add(oneOfGeneration);

          } // "#Life 1.06"
          else throw new Exception("Not good file format or file");
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
//    System.out.println(generations.getPresentGeneration().size());
    return generations;
  }

  public static List<Coordinate> makeNeighborList(Coordinate baseCoordinate) {
    List<Coordinate> neighborList = new ArrayList<>();

    Coordinate startCoordinate
            = new Coordinate(baseCoordinate.getX() - 1, baseCoordinate.getY() - 1);

    for (int y = 0; y < 3; y++) {
      for (int x = 0; x < 3; x++) {
        Coordinate neighbor
                = new Coordinate(startCoordinate.getX() + x, startCoordinate.getY() + y);
        if (y != 1 || x != 1) {
          neighborList.add(neighbor);
        }

      }
    }

    return neighborList;
  }

  public static void makeNewGenerations(Generations generation) {

//    A sejt elpusztul, ha kettőnél kevesebb (elszigetelődés),
//vagy háromnál több (túlnépesedés) szomszédja van.
//  Új sejt születik minden olyan cellában,
//  melynek környezetében pontosan három sejt található.
    generation.getDyingGeneration().clear();
    generation.getNewGeneration().clear();

    for (int i = 0; i < generation.getPresentGeneration().size(); i++) {
      Coordinate stillAliveCoordinate = generation.getPresentGeneration().get(i);
      List<Coordinate> neighborList = makeNeighborList(stillAliveCoordinate);
      int aliveNeighbor = 0;
      for (int j = 0; j < neighborList.size(); j++) {
        Coordinate neighbor = neighborList.get(j);

        if (generation.getPresentGeneration().contains(neighbor)) {
          aliveNeighbor++;
        } else {
//          vizsgáljuk meg születésre
          if (!generation.getNewGeneration().contains(neighbor)) {
            if (willBeNew(neighbor, generation)) {
              generation.getNewGeneration().add(neighbor);
            }
          }
        }
      }
      if (aliveNeighbor < 2 || aliveNeighbor > 3) {
        generation.getDyingGeneration().add(stillAliveCoordinate);
      }
    }

    generation.getPresentGeneration().removeAll(generation.getDyingGeneration());
    generation.getPresentGeneration().addAll(generation.getNewGeneration());
  }

  public static boolean willBeNew(Coordinate empty, Generations generation) {

    List<Coordinate> neighborList = makeNeighborList(empty);
    int aliveNeighbor = 0;
    for (int j = 0; j < neighborList.size(); j++) {
      Coordinate neighbor = neighborList.get(j);
      if (generation.getPresentGeneration().contains(neighbor)) {
        aliveNeighbor++;
      }
    }
    //  Új sejt születik minden olyan cellában,
//  melynek környezetében pontosan három sejt található.

    return aliveNeighbor == 3 
            && (empty.getX()<=SizeAndScale.LIFE_PLACE_X_MAX/2 && empty.getX()>=SizeAndScale.LIFE_PLACE_X_MAX/-2)
            && (empty.getY()<=SizeAndScale.LIFE_PLACE_Y_MAX/2 && empty.getY()>=SizeAndScale.LIFE_PLACE_Y_MAX/-2);
  }
}
