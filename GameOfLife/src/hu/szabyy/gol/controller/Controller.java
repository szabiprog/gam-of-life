package hu.szabyy.gol.controller;

import hu.szabyy.gol.model.Generations;
import hu.szabyy.gol.services.ServicesOfGol;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Controller {

  static final Controller INSTANCE = new Controller();

  private ServicesOfGol services;

  private Generations generations;
  private List<Generations> historyOfGenerations;
  private Boolean started;
  private Boolean paused;
  private Integer stepCounter;
  private Boolean changedGeneration;
  private Integer scaleToView;
  private Boolean reverse;

  public Controller() {

    this.generations = new Generations();
    historyOfGenerations = new ArrayList<>();
    initValues();
    this.scaleToView = 1;

  }

  public List<Generations> getHistoryOfGenerations() {
    return historyOfGenerations;
  }

  public Integer getScaleToView() {
    return scaleToView;
  }

  public void setScaleToView(Integer scaleToView) {
    this.scaleToView = scaleToView;
  }

  public Boolean isReverse() {
    return reverse;
  }

  public void setReverse(Boolean reverse) {
    this.reverse = reverse;
  }

  public Boolean isChangedGeneration() {
    return changedGeneration;
  }

  public Integer getStepCounter() {
    return stepCounter;
  }

  public void setStepCounter(Integer stepCounter) {
    this.stepCounter = stepCounter;
  }

  public Boolean getStarted() {
    return this.started;
  }

  public void setStarted(Boolean started) {
    this.started = started;
  }

  public Boolean getPaused() {
    return this.paused;
  }

  public void setPaused(Boolean paused) {
    this.paused = paused;
  }

  public static Controller getINSTANCE() {
    return Controller.INSTANCE;
  }

  public ServicesOfGol getServices() {
    return this.services;
  }

  public void setServices(ServicesOfGol services) {
    this.services = services;
  }

  public Generations getGenerations() {
    return this.generations;
  }

  public void setGenerations(Generations generations) {
    historyOfGenerations = new ArrayList<>();
    this.generations = generations;
    initValues();
  }

  public void setGenerationsFromFile(File file) throws Exception {
    historyOfGenerations = new ArrayList<>();
    this.generations = ServicesOfGol.fileToGenerations(file);
    initValues();
  }

  public Generations doIt() {
    Generations oldGenerations = saveOldGenerations();
    if (!isReverse()) {
      ServicesOfGol.makeNewGenerations(this.generations);
      if (!this.generations.getDyingGeneration().isEmpty()
              || !this.generations.getNewGeneration().isEmpty()) {
        this.setStepCounter(this.getStepCounter() + 1);
        this.historyOfGenerations.add(oldGenerations);
        
      } else {
        this.changedGeneration = false;
      }
    } else {
      if (this.historyOfGenerations.size() > 0) {
        this.generations.setPresentGeneration(
                (this.historyOfGenerations.get(this.historyOfGenerations.size() - 1)).getPresentGeneration());
        this.historyOfGenerations.remove(this.historyOfGenerations.get(this.historyOfGenerations.size() - 1));

        setStepCounter(this.historyOfGenerations.size());
      } 
    }

    return oldGenerations;
  }

  private Generations saveOldGenerations() {
    Generations oldGenerations = new Generations();
    oldGenerations.getPresentGeneration().addAll(this.getGenerations().getPresentGeneration());
    return oldGenerations;
  }

  private void initValues() {
    this.stepCounter = 0;
    this.started = false;
    this.paused = true;
    this.changedGeneration = true;
    this.reverse = false;
  }
}
